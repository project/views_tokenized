<?php

namespace Drupal\views_tokenized;

use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\Core\Utility\Token;
use Drupal\views\ViewExecutable;

/**
 * Service for Token replacements in Views.
 */
class ViewsTokenized {

  /**
   * The Token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|null
   */
  protected $entityTypeManager;

  /**
   * The context repository.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface|null
   */
  protected $contextRepository;

  /**
   * Statically cached context IDs.
   *
   * @var string[]
   */
  protected static $contextIds;

  /**
   * A list of statically cached tokens that were already scanned.
   *
   * @var array
   */
  protected $scanned = [];

  /**
   * The ViewsTokenized constructor.
   *
   * @param \Drupal\Core\Utility\Token $token
   *   The Token service.
   */
  public function __construct(Token $token) {
    $this->token = $token;
  }

  /**
   * Query alteration that replaces any Tokens.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   A Query object describing the composite parts of a SQL query.
   */
  public function alterQuery(AlterableInterface $query) {
    $to_replace = [];
    $view = $query->getMetaData('view');
    if (!($view instanceof ViewExecutable)) {
      return;
    }
    $this->collectTokenInput($query, $to_replace);
    if (empty($to_replace)) {
      return;
    }

    if (!isset($this->entityTypeManager, $this->contextRepository)) {
      $this->entityTypeManager = \Drupal::entityTypeManager();
      $this->contextRepository = \Drupal::service('context.repository');
    }
    // Use any global context that provides a value as token data.
    $token_data = [];
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $bubbleable_metadata = BubbleableMetadata::createFromRenderArray($view->element);
    $previous_cache_contexts = $bubbleable_metadata->getCacheContexts();
    /** @var \Drupal\token\TokenEntityMapperInterface $entity_token_type_mapper */
    $entity_token_type_mapper = \Drupal::hasService('token.entity_mapper') ? \Drupal::service('token.entity_mapper') : NULL;
    if (!isset(static::$contextIds)) {
      static::$contextIds = array_keys($this->contextRepository->getAvailableContexts());
    }

    $contexts = [];
    if (!empty(static::$contextIds)) {
      foreach ($this->contextRepository->getRuntimeContexts(static::$contextIds) as $context) {
        $context_data_type = explode(':', $context->getContextDefinition()->getDataType());
        // For "entity:*" data types, we skip its prefixes and only respect
        // the last entry of the data type string (e.g. for "entity:node", we
        // want to only use "node").
        $type = end($context_data_type);
        $value = $context->hasContextValue() ? $context->getContextValue() : NULL;
        // We add the context to the array, even if the value is not being used.
        // That way cacheability metadata will include cache contexts that are
        // required for variations, especially when global context is being
        // used elsewhere.
        $contexts[$type] = $context;
        if (isset($value) && !isset($token_data[$type])) {
          $token_data[$type] = $value;
        }
      }
    }
    // Switch to translations if necessary and available.
    // Also re-map entities that may have a different token type defined.
    foreach ($token_data as $type => $value) {
      if (($value instanceof TranslatableInterface) && ($value->language()->getId() !== $langcode) && ($value->hasTranslation($langcode))) {
        $value = $value->getTranslation($langcode);
        $token_data[$type] = $value;
      }
      if (!($value instanceof EntityInterface)) {
        continue;
      }
      /** @var \Drupal\Core\Entity\EntityInterface $value */
      if ($entity_token_type_mapper) {
        $the_real_token_type = $entity_token_type_mapper->getTokenTypeForEntityType($value->getEntityTypeId(), TRUE);
      }
      else {
        $definition = $this->entityTypeManager->getDefinition($value->getEntityTypeId());
        $the_real_token_type = $definition->get('token_type') ?: $value->getEntityTypeId();
      }
      if (!isset($token_data[$the_real_token_type])) {
        if (isset($contexts[$type])) {
          $contexts[$the_real_token_type] = $contexts[$type];
        }
        $token_data[$the_real_token_type] = $value;
      }
    }
    // Filter out data that is not being used and is not allowed to be viewed.
    // Also add cacheability metadata of data that is being used.
    foreach ($token_data as $type => $value) {
      $type_is_used = FALSE;
      foreach ($this->scanned as $tokens) {
        foreach (array_keys($tokens) as $used_token_type) {
          if ($used_token_type === $type) {
            $type_is_used = TRUE;
            break 2;
          }
        }
      }
      if (!$type_is_used) {
        unset($token_data[$type]);
        continue;
      }
      if ($value instanceof CacheableDependencyInterface) {
        $bubbleable_metadata->addCacheableDependency($value);
      }
      if (isset($contexts[$type])) {
        $bubbleable_metadata->addCacheableDependency($contexts[$type]);
      }
      if ($value instanceof AccessibleInterface) {
        /** @var \Drupal\Core\Access\AccessResultInterface $access_result */
        $access_result = $value->access('view', NULL, TRUE);
        $bubbleable_metadata->addCacheableDependency($access_result);
        if (!$access_result->isAllowed()) {
          unset($token_data[$type]);
          continue;
        }
      }
    }
    foreach ($to_replace as &$item) {
      $item['text'] = trim($this->token->replace($item['text'], $token_data, ['langcode' => $langcode], $bubbleable_metadata));
    }
    // @todo Resolve this to be cacheable by respecting cache contexts within
    // \Drupal\views\Plugin\views\cache\CachePluginBase::generateResultsKey().
    // @see https://www.drupal.org/project/views_tokenized/issues/3253880
    if (array_diff($bubbleable_metadata->getCacheContexts(), $previous_cache_contexts)) {
      $bubbleable_metadata->mergeCacheMaxAge(0);
    }
    $bubbleable_metadata->applyTo($view->element);
  }

  /**
   * Collects input containing Tokens from the given query.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   A Query object describing the composite parts of a SQL query.
   * @param array &$to_replace
   *   A list of already collected input to evaluate.
   */
  protected function collectTokenInput(AlterableInterface $query, array &$to_replace) {
    /** @var \Drupal\Core\Database\Query\Select $query */
    $tables = &$query->getTables();
    $conditions = &$query->conditions();

    foreach ($tables as $table_name => $table_metadata) {
      if (empty($table_metadata['arguments'])) {
        continue;
      }
      foreach ($table_metadata['arguments'] as $replacement_key => $value) {
        if (is_array($value)) {
          foreach ($value as $sub_key => $sub_value) {
            if ($hash = $this->containsToken($sub_value)) {
              $to_replace[] = [
                'hash' => $hash,
                'text' => &$tables[$table_name]['arguments'][$replacement_key][$sub_key],
              ];
            }
          }
        }
        elseif ($hash = $this->containsToken($value)) {
          $to_replace[] = [
            'hash' => $hash,
            'text' => &$tables[$table_name]['arguments'][$replacement_key],
          ];
        }
      }
    }

    $this->collectFromConditions($query, $conditions, $to_replace);
  }

  /**
   * Sub-method to collect input contaning Tokens from conditions.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   A Query object describing the composite parts of a SQL query.
   * @param mixed &$conditions
   *   The conditions.
   * @param array &$to_replace
   *   A list of already collected input to evaluate.
   */
  protected function collectFromConditions(AlterableInterface $query, &$conditions, array &$to_replace) {
    foreach ($conditions as $condition_id => &$condition) {
      if (!is_numeric($condition_id)) {
        continue;
      }
      if (is_string($condition['field']) && ($hash = $this->containsToken($condition['field']))) {
        $to_replace[] = [
          'hash' => $hash,
          'text' => &$condition['field'],
        ];
      }
      elseif (is_object($condition['field'])) {
        $sub_conditions = &$condition['field']->conditions();
        $this->collectFromConditions($query, $sub_conditions, $to_replace);
      }
      if (is_object($condition['value'])) {
        $this->collectTokenInput($condition['value'], $to_replace);
      }
      elseif (isset($condition['value'])) {
        if (is_array($condition['value'])) {
          foreach ($condition['value'] as $key => $value) {
            if (is_string($value) && ($hash = $this->containsToken($value))) {
              $to_replace[] = [
                'hash' => $hash,
                'text' => &$condition['value'][$key],
              ];
            }
          }
        }
        elseif (is_string($condition['value']) && ($hash = $this->containsToken($condition['value']))) {
          $to_replace[] = [
            'hash' => $hash,
            'text' => &$condition['value'],
          ];
        }
      }
    }
  }

  /**
   * Checks whether the given string contains at least one Token.
   *
   * @param string $string
   *   The string to check for.
   *
   * @return string|false
   *   Returns the hash of the string if it contains at least one Token, FALSE
   *   otherwise.
   */
  public function containsToken($string) {
    if (is_object($string) && method_exists($string, '__toString')) {
      $string = (string) $string;
    }
    if (!is_string($string) || strpos($string, '[') === FALSE || !strpos($string, ':') || !strpos($string, ']')) {
      return FALSE;
    }
    $hash = hash('md4', $string);
    if (!isset($this->scanned[$hash])) {
      $this->scanned[$hash] = $this->token->scan($string);
    }
    return !empty($this->scanned[$hash]) ? $hash : FALSE;
  }

}
