# Views Tokenized

**Use Tokens within Views arguments and filters.**

## 0. Contents

* 1. Installation
* 2. Usage
* 3. Recommended projects
* 4. Reporting problems
* 5. Maintainers

## 1. Installation

* This module requires no modules outside of Drupal core.
* The Views module of Drupal core must be installed.
* Install the module as you would normally install a contributed Drupal module.
  Visit https://www.drupal.org/node/1897420 for further information.

## 2. Usage

Once installed, you can use globally available Tokens within Views arguments,
like contextual filters but also regular filters on fields.

**Please note**:
When using Tokens from data that are provided by a certain context, for example
using the node coming from the URL, then you need to do either one of the
following:
* Disable caching for the View (click on `Caching:` within Views UI display
config and choose `None`) or...
* ...add a contextual filter for it. You can do so by choosing `Global: Null`
within contextual filters, and then select "Provide a default value" ->
"Content ID from URL". For the reason why this is necessary, have a look at
https://www.drupal.org/project/views_tokenized/issues/3253880.

## 3. Recommended projects

If you need Tokens within text formats, use the Token Filter module:
https://www.drupal.org/project/token_filter

If you need more advanced logic, for example simple conditions, have a look at
the Mustache Logic-less Templates module:
https://www.drupal.org/project/mustache_templates

If you need a node, user, taxonomy or any other token from a given context,
have a look at ECA: https://www.drupal.org/project/eca
With this module you can react on certain events in Drupal, for example when
a node is being viewed, and load the according object into the Token system.

## 4. Reporting problems

Post into the issue queue of this project:
https://www.drupal.org/project/issues/views_tokenized

## 5. Maintainers

* Maximilian Haupt (https://www.drupal.org/u/mxh)
